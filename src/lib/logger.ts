import { PublishCase } from './aplication/publish';
import { ClearCase } from './aplication/clear';

import { IDsLogger } from './domain/types/logger.type';
import { ITagPrimitive } from './domain/types/tag.type';
import { ILevel } from './domain/types/level.type';
import { IPublisher } from './domain/types/publisher.type';
import { ISettingPrimitives } from './domain/types/setting.type';

export class DsLogger implements IDsLogger {
    private publishers: IPublisher[];

    private config: ISettingPrimitives;

    constructor(config?: ISettingPrimitives) {
        this.publishers = [];
        this.config = config ?? {};
    }

    public addPublisher(publicsher: IPublisher): this {
        this.publishers.push(publicsher);
        return this;
    }

    public log(msg: string, extraparams?: ITagPrimitive[]): void {
        this.writeToLog(msg, 'all', extraparams);
    }

    public debug(msg: string, extraparams?: ITagPrimitive[]): void {
        this.writeToLog(msg, 'debug', extraparams);
    }

    public info(msg: string, extraparams?: ITagPrimitive[]): void {
        this.writeToLog(msg, 'info', extraparams);
    }

    public notice(msg: string, extraparams?: ITagPrimitive[]): void {
        this.writeToLog(msg, 'notice', extraparams);
    }

    public warn(msg: string, extraparams?: ITagPrimitive[]): void {
        this.writeToLog(msg, 'warn', extraparams);
    }

    public error(msg: string, extraparams?: ITagPrimitive[]): void {
        this.writeToLog(msg, 'error', extraparams);
    }

    public critical(msg: string, extraparams?: ITagPrimitive[]): void {
        this.writeToLog(msg, 'crit', extraparams);
    }

    public clear(): void {
        new ClearCase(this.publishers).run();
    }

    private writeToLog(msg: string, level: ILevel, extraparams?: ITagPrimitive[]) {
        new PublishCase(this.publishers, this.config).run(msg, level, extraparams);
    }
}
