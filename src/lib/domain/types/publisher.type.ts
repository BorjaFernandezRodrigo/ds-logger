import { ILevel } from './level.type';

export interface IPublisher {
    log: (msg: JSON | string, level: ILevel) => void;
    clear: () => void;
    canHandler: (level: ILevel) => boolean;
}
