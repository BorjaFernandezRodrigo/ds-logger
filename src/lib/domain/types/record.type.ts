export interface IRecord {
    msg: string;
    level: string;
    date?: Date;
    [key: string]: unknown;
}
