export interface ITagPrimitive {
    name: string;
    value: string;
}
