import { FormatValueObject } from '../value-object/format.object';
import { TagValueObject } from '../value-object/tag.object';
import { WithDateValueObject } from '../value-object/with-date.object';
import { IFormat } from './format.type';
import { ITagPrimitive } from './tag.type';

export interface ISetting {
    format?: FormatValueObject;
    withDate?: WithDateValueObject;
    tags?: TagValueObject[];
}

export interface ISettingPrimitives {
    format?: IFormat;
    withDate?: boolean;
    tags?: ITagPrimitive[];
}
