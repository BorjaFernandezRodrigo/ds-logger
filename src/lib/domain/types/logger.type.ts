import { ITagPrimitive } from './tag.type';

export interface IDsLogger {
    log: (msg: string, extraparams?: ITagPrimitive[]) => void;
    debug: (msg: string, extraparams?: ITagPrimitive[]) => void;
    info: (msg: string, extraparams?: ITagPrimitive[]) => void;
    notice: (msg: string, extraparams?: ITagPrimitive[]) => void;
    warn: (msg: string, extraparams?: ITagPrimitive[]) => void;
    error: (msg: string, extraparams?: ITagPrimitive[]) => void;
    clear: (msg: string, extraparams?: ITagPrimitive[]) => void;
}
