export enum Levels {
    'all' = 0,
    'debug' = 1,
    'info' = 2,
    'notice' = 3,
    'warn' = 4,
    'error' = 5,
    'crit' = 6,
    'alert' = 7,
    'emerg' = 8,
}
