import { IRecord } from '../types/record.type';
import { ISetting } from '../types/setting.type';
import { LevelValueObject } from '../value-object/level.object';
import { TagValueObject } from '../value-object/tag.object';

export class RecordService {
    constructor(private config: ISetting) {}

    public buildRecord(msg: string, level: LevelValueObject, extraparams?: TagValueObject[]): JSON | string {
        const extraparamsFinal = this.buildExtraParams(extraparams);
        let record: IRecord = {
            level: level.value,
            date: this.config.withDate?.value ? new Date() : undefined,
            msg,
        };
        record = this.buildTags(record, this.config?.tags);
        if (extraparamsFinal) record = this.buildTags(record, extraparamsFinal);
        return this.config?.format?.value === 'string' ? this.onString(record) : this.onJson(record);
    }

    private buildExtraParams(extraparams?: TagValueObject[]) {
        return extraparams?.map((extraparam) => new TagValueObject(extraparam.name, extraparam.value));
    }

    private buildTags(record: IRecord, tags?: TagValueObject[]): IRecord {
        tags?.forEach((extraParam: TagValueObject) => {
            record[extraParam.name] = extraParam.value;
        });
        return record;
    }

    private onJson(record: IRecord): JSON {
        return JSON.parse(JSON.stringify(record));
    }

    private onString(record: IRecord): string {
        let msg = record.date ? `${record.date.toISOString()} - ` : '';
        msg = msg + `[${record.level}]: `;
        Object.keys(record).forEach((key) => {
            if (key !== 'level' && key !== 'date' && key !== 'msg') msg = msg + `${key}: ${record[key]} | `;
        });
        msg = msg + `Menssage: ${record.msg}`;
        return msg;
    }
}
