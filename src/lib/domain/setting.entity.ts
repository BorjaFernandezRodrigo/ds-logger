import { FormatValueObject } from './value-object/format.object';
import { ISettingPrimitives, ISetting } from './types/setting.type';
import { TagValueObject } from './value-object/tag.object';
import { WithDateValueObject } from './value-object/with-date.object';

export class SettingEntity implements ISetting {
    public format: FormatValueObject;

    public withDate: WithDateValueObject;

    public tags: TagValueObject[];

    constructor(logSettings?: ISettingPrimitives) {
        this.format = new FormatValueObject(logSettings?.format);
        this.withDate = new WithDateValueObject(logSettings?.withDate);
        this.tags = logSettings?.tags?.map((tag) => new TagValueObject(tag.name, tag.value)) ?? [];
    }
}
