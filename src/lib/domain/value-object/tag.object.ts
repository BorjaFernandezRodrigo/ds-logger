import { ITagPrimitive } from '../types/tag.type';
import { ValueObject } from './value.object';

export class TagValueObject extends ValueObject<string> implements ITagPrimitive {
    readonly name: string;

    constructor(name: string, value: string) {
        super(value);
        this.name = name;
    }
}
