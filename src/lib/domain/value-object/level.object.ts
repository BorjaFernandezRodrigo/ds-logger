import { ILevel } from '../types/level.type';
import { ValueObject } from './value.object';

export class LevelValueObject extends ValueObject<ILevel> {
    constructor(value?: ILevel) {
        super(value ?? 'debug');
    }
}
