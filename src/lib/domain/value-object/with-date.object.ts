import { ValueObject } from './value.object';

export class WithDateValueObject extends ValueObject<boolean> {
    constructor(value: boolean = true) {
        super(value);
    }
}
