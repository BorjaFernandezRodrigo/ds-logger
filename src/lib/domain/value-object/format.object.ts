import { IFormat } from '../types/format.type';
import { ValueObject } from './value.object';

export class FormatValueObject extends ValueObject<IFormat> {
    constructor(value?: IFormat) {
        super(value ?? 'JSON');
    }
}
