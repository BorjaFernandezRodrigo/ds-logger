// eslint-disable-next-line @typescript-eslint/ban-types
export type Primitives = string | number | boolean | Date;

export abstract class ValueObject<T extends Primitives> {
    readonly value: T;

    constructor(value: T) {
        this.value = value;
    }
}
