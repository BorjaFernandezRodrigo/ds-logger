import { IPublisher } from '../domain/types/publisher.type';

export class ClearCase {
    constructor(public repositories: IPublisher[]) {}

    public run(): void {
        this.repositories.forEach((publisher) => {
            publisher.clear();
        });
    }
}
