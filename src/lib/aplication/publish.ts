import { ISettingPrimitives, ITagPrimitive } from '../public.api';

import { IPublisher } from '../domain/types/publisher.type';
import { ILevel } from '../domain/types/level.type';
import { ISetting } from '../domain/types/setting.type';
import { SettingEntity } from '../domain/setting.entity';
import { LevelValueObject } from '../domain/value-object/level.object';
import { RecordService } from '../domain/service/record.service';

export class PublishCase {
    private config: ISetting;

    private repositories: IPublisher[];

    constructor(repositories?: IPublisher[], config?: ISettingPrimitives) {
        this.config = new SettingEntity(config);
        this.repositories = repositories ?? [];
    }

    public run(msg: string, level?: ILevel, extraparams?: ITagPrimitive[]) {
        const levelValueObject = new LevelValueObject(level);
        if (this.repositories.length !== 0) {
            const record = new RecordService(this.config).buildRecord(msg, levelValueObject, extraparams);
            this.publish(record, levelValueObject);
        }
    }

    private publish(record: string | JSON, level: LevelValueObject) {
        this.repositories.forEach((publisher) => {
            if (publisher.canHandler(level?.value)) {
                publisher.log(record, level.value);
            }
        });
    }
}
