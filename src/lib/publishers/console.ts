import { IPublisher } from '../domain/types/publisher.type';
import { ILevel } from '../domain/types/level.type';
import { Levels } from '../domain/levels.enum';

export class ConsolePublisher implements IPublisher {
    private readonly level: ILevel;

    constructor(level: ILevel) {
        this.level = level;
    }

    public log(msg: JSON | string, level: ILevel) {
        if (level === 'error' || level === 'crit') {
            console.error(msg);
            return;
        }
        if (level === 'warn') {
            console.warn(msg);
            return;
        }
        if (level === 'info') {
            console.info(msg);
            return;
        }
        if (level === 'debug') {
            console.debug(msg);
            return;
        }
        console.log(msg);
    }

    public clear() {
        console.clear();
    }

    public canHandler(level: ILevel): boolean {
        return Levels[this.level] <= Levels[level];
    }
}
