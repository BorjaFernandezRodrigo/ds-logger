import { IPublisher } from '../domain/types/publisher.type';
import { ILevel } from '../domain/types/level.type';
import { Levels } from '../domain/levels.enum';

export class LocalStoragePublisher implements IPublisher {
    private readonly level: ILevel;

    private readonly location: string;

    constructor(level: ILevel, location: string) {
        this.level = level;
        this.location = location;
    }

    public log(msg: JSON | string) {
        const values = JSON.parse(localStorage.getItem(this.location) ?? '[]') ?? [];
        values.push(msg);
        localStorage.setItem(this.location, JSON.stringify(values));
    }

    public clear() {
        localStorage.removeItem(this.location);
    }

    public canHandler(level: ILevel): boolean {
        return Levels[this.level] <= Levels[level];
    }
}
