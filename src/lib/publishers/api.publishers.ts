import { IPublisher } from '../domain/types/publisher.type';
import { ILevel } from '../domain/types/level.type';
import { Levels } from '../domain/levels.enum';

export class ApiPublisher implements IPublisher {
    private readonly level: ILevel;

    private readonly location: string;

    constructor(level: ILevel, location: string) {
        this.level = level;
        this.location = location;
    }

    public log(msg: JSON | string) {
        fetch(this.location, { method: 'POST', body: JSON.stringify(msg) });
    }

    public clear() {
        fetch(this.location, { method: 'DELETE' });
    }

    public canHandler(level: ILevel): boolean {
        return Levels[this.level] <= Levels[level];
    }
}
