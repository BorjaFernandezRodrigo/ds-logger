import * as fs from 'fs';
import { Levels } from '../domain/levels.enum';
import { ILevel, IPublisher } from '../public.api';

export class FilePublisher implements IPublisher {
    private readonly fileSystem: typeof fs;

    private readonly level: ILevel;

    private readonly location: string;

    constructor(level: ILevel, location: string) {
        this.level = level;
        this.fileSystem = fs;
        this.location = location;
    }

    public log(msg: JSON | string) {
        this.fileSystem.appendFile(`${this.location}`, `${JSON.stringify(msg)}\n`, () => {});
    }

    public clear() {
        this.fileSystem.unlinkSync(`${this.location}`);
    }

    public canHandler(level: ILevel): boolean {
        return Levels[this.level] <= Levels[level];
    }
}
//
