import * as path from 'path';

import { ConsolePublisher, ILevel, DsLogger, LocalStoragePublisher, FilePublisher } from '../../lib/public.api';
import { describe, test, expect, jest } from '@jest/globals';

describe('DsLogger', () => {
    jest.useFakeTimers().setSystemTime(new Date('1970-01-01'));

    test('Deberia escribir logs en consola y despues limpiar el stack', () => {
        const msg = 'Test message';
        const level: ILevel = 'all';
        const consolePublisher = new ConsolePublisher(level);
        const logger = new DsLogger().addPublisher(consolePublisher);

        console.info = jest.fn();
        logger.info(msg);
        expect(console.info).toHaveBeenCalledTimes(1);
        expect(console.info).toHaveBeenCalledWith({ date: '1970-01-01T00:00:00.000Z', level: 'info', msg: 'Test message' });

        console.log = jest.fn();
        logger.log(msg);
        expect(console.log).toHaveBeenCalledTimes(1);
        expect(console.log).toHaveBeenCalledWith({ date: '1970-01-01T00:00:00.000Z', level: 'all', msg: 'Test message' });

        console.debug = jest.fn();
        logger.debug(msg);
        expect(console.debug).toHaveBeenCalledTimes(1);
        expect(console.debug).toHaveBeenCalledWith({ date: '1970-01-01T00:00:00.000Z', level: 'debug', msg: 'Test message' });

        console.log = jest.fn();
        logger.notice(msg);
        expect(console.log).toHaveBeenCalledTimes(1);
        expect(console.log).toHaveBeenCalledWith({ date: '1970-01-01T00:00:00.000Z', level: 'notice', msg: 'Test message' });

        console.warn = jest.fn();
        logger.warn(msg);
        expect(console.warn).toHaveBeenCalledTimes(1);
        expect(console.warn).toHaveBeenCalledWith({ date: '1970-01-01T00:00:00.000Z', level: 'warn', msg: 'Test message' });

        console.error = jest.fn();
        logger.error(msg);
        expect(console.error).toHaveBeenCalledTimes(1);
        expect(console.error).toHaveBeenCalledWith({ date: '1970-01-01T00:00:00.000Z', level: 'error', msg: 'Test message' });

        console.error = jest.fn();
        logger.critical(msg);
        expect(console.error).toHaveBeenCalledTimes(1);
        expect(console.error).toHaveBeenCalledWith({ date: '1970-01-01T00:00:00.000Z', level: 'crit', msg: 'Test message' });

        console.clear = jest.fn();
        logger.clear();
        expect(console.clear).toHaveBeenCalledTimes(1);
    });

    test('Deberia escribir logs en el local storage y despues limpiar el item', () => {
        const msg = 'Test message';
        const level: ILevel = 'all';
        const localStoragePublisher = new LocalStoragePublisher(level, 'test');
        const logger = new DsLogger().addPublisher(localStoragePublisher);

        logger.log(msg);
        expect(localStorage.setItem).toHaveBeenCalledTimes(1);
        expect(localStorage.setItem).toHaveBeenCalledWith(
            'test',
            JSON.stringify([{ level: 'all', date: '1970-01-01T00:00:00.000Z', msg: 'Test message' }])
        );

        logger.clear();
        expect(localStorage.removeItem).toHaveBeenCalledTimes(1);
    });

    test('Deberia escribir logs en un fichero y despues borrarlo', () => {
        const msg = 'Test message';
        const level: ILevel = 'all';

        const pathConfig = path.join('dist/tests/test.log');
        const filePublisher = new FilePublisher(level, pathConfig);
        const logger = new DsLogger().addPublisher(filePublisher);

        logger.log(msg);
        expect(localStorage.setItem).toHaveBeenCalledTimes(1);
        expect(localStorage.setItem).toHaveBeenCalledWith(
            'test',
            JSON.stringify([{ level: 'all', date: '1970-01-01T00:00:00.000Z', msg: 'Test message' }])
        );

        logger.clear();
        expect(localStorage.removeItem).toHaveBeenCalledTimes(1);
    });
});
