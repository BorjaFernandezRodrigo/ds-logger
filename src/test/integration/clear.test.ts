import { IPublisher } from '../../lib/public.api';
import { describe, test, jest, expect } from '@jest/globals';
import { ClearCase } from '../../lib/aplication/clear';

describe('ClearCase', () => {
    test("should call the 'clear' method on all repositories passed to the constructor", () => {
        const repositories: IPublisher[] = [
            {
                log: jest.fn(),
                clear: jest.fn(),
                canHandler: jest.fn(() => {
                    return true;
                }),
            },
        ];
        const clearCase = new ClearCase(repositories);
        clearCase.run();
        repositories.forEach((repository) => {
            expect(repository.clear).toHaveBeenCalled();
        });
    });
});
