import { PublishCase } from '../../lib/aplication/publish';
import { describe, expect, jest, test } from '@jest/globals';
import { ILevel } from '../../lib/public.api';
import { Levels } from '../../lib/domain/levels.enum';

describe('PublishCase', () => {
    jest.useFakeTimers().setSystemTime(new Date('1970-01-01'));

    test('Deberia funcionar correctanmente cuando no se le pasa publishers', () => {
        const publishCase = new PublishCase();
        const msg = 'Test message';
        const level = 'info';
        publishCase.run(msg, level);
    });

    test('Deberia registrar el mensaje en todos los repositorios que puedan manejarlo', () => {
        const repository1 = {
            log: jest.fn(),
            clear: jest.fn(),
            canHandler: jest.fn((level: ILevel) => {
                return Levels.debug <= Levels[level];
            }),
        };
        const repository2 = {
            log: jest.fn(),
            clear: jest.fn(),
            canHandler: jest.fn((level: ILevel) => {
                return Levels.error <= Levels[level];
            }),
        };
        const publishCase = new PublishCase([repository1, repository2]);
        const msg = 'Test message';
        const level: ILevel = 'info';
        publishCase.run(msg, level);
        expect(repository1.log).toHaveBeenCalledTimes(1);
        expect(repository1.log).toHaveBeenCalledWith(
            {
                level: 'info',
                date: new Date().toISOString(),
                msg: 'Test message',
            },
            'info'
        );
        expect(repository2.log).not.toHaveBeenCalled();
    });
});
