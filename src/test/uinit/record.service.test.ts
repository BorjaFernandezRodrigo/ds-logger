import { describe, test, expect, jest } from '@jest/globals';
import { ISetting } from '../../lib/domain/types/setting.type';
import { RecordService } from '../../lib/domain/service/record.service';
import { LevelValueObject } from '../../lib/domain/value-object/level.object';
import { TagValueObject } from '../../lib/domain/value-object/tag.object';
import { WithDateValueObject } from '../../lib/domain/value-object/with-date.object';
import { FormatValueObject } from '../../lib/domain/value-object/format.object';

describe('RecordService', () => {
    jest.useFakeTimers().setSystemTime(new Date(Date.UTC(1970, 0, 1, 0, 0, 0)));

    process.env.TZ = 'en-En';
    test('Deberia construir un registro log sin configuracion.', () => {
        const config: ISetting = {};
        const recordService = new RecordService(config);
        const result = recordService.buildRecord('Test message', new LevelValueObject());
        expect(result).toEqual({
            level: 'debug',
            msg: 'Test message',
        });
    });

    test('Deberia construirse un registro con tags', () => {
        const config: ISetting = {
            tags: [new TagValueObject('tag1', 'value1'), new TagValueObject('tag2', 'value2')],
        };
        const recordService = new RecordService(config);
        const result = recordService.buildRecord('Test message', new LevelValueObject('info'));
        expect(result).toEqual({
            level: 'info',
            msg: 'Test message',
            tag1: 'value1',
            tag2: 'value2',
        });
    });

    test('Deberia construirse un registro con fecha', () => {
        const config: ISetting = {
            withDate: new WithDateValueObject(true),
        };
        const recordService = new RecordService(config);
        const result = recordService.buildRecord('Test message', new LevelValueObject('info'));
        expect(result).toEqual({
            date: new Date().toISOString(),
            level: 'info',
            msg: 'Test message',
        });
    });
    test('Deberia construirse en formato string, con fecha, con tags y con extra params', () => {
        const extraParams: TagValueObject[] = [new TagValueObject('param1', 'value1'), new TagValueObject('param2', 'value2')];
        const config: ISetting = {
            tags: [new TagValueObject('tag1', 'value1'), new TagValueObject('tag2', 'value2')],
            format: new FormatValueObject('string'),
            withDate: new WithDateValueObject(true),
        };
        const recordService = new RecordService(config);
        const result = recordService.buildRecord('Test message', new LevelValueObject('info'), extraParams);
        expect(result).toEqual(
            // eslint-disable-next-line max-len
            '1970-01-01T00:00:00.000Z - [info]: tag1: value1 | tag2: value2 | param1: value1 | param2: value2 | Menssage: Test message'
        );
    });

    test('Deberia construirse un registro en formato string sin fecha', () => {
        const config: ISetting = {
            format: new FormatValueObject('string'),
            withDate: new WithDateValueObject(false),
        };
        const recordService = new RecordService(config);
        const result = recordService.buildRecord('Test message', new LevelValueObject('info'));
        expect(result).toEqual('[info]: Menssage: Test message');
    });
});
