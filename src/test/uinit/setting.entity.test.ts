import { describe, test, expect } from '@jest/globals';
import { SettingEntity } from '../../lib/domain/setting.entity';

describe('SettingEntity', () => {
    test('Deberia crear una entidad settings sin pasarle configuracion inicial.', () => {
        const settingEntity = new SettingEntity();
        expect(settingEntity.format.value).toEqual('JSON');
        expect(settingEntity.withDate.value).toEqual(true);
        expect(settingEntity.tags).toEqual([]);
    });

    test('Deberia crear una entidad settings con formato string.', () => {
        const settingEntity = new SettingEntity({
            format: 'string',
        });
        expect(settingEntity.format.value).toEqual('string');
    });

    test('Deberia crear una entidad settings con fecha.', () => {
        const settingEntity = new SettingEntity({
            withDate: false,
        });
        expect(settingEntity.withDate.value).toEqual(false);
    });

    test('Deberia crear una entidad settings con formato string.', () => {
        const tags = [
            {
                name: 'name',
                value: 'value',
            },
        ];
        const settingEntity = new SettingEntity({
            tags,
        });
        expect(settingEntity.tags).toEqual(tags);
    });
});
