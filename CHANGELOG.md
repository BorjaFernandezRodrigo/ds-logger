# Registro de cambios

Todos los cambios de la libreira, se integraran en este docuemnto.

---

> Los tipos que se tendran encuenta en este registro son:
>
> -   `Added` funcionalidades nuevas.
> -   `Changed` cambios en las funcionalidades existentes.
> -   `Deprecated` indicar que una característica o funcionalidad está obsoleta y que se eliminará en las próximas versiones.
> -   `Removed` características en desuso que se eliminaron en esta versión.
> -   `Fixed` corrección de errores.
> -   `Security` vulnerabilidades.

> Notacion de versionado semantico utilizado:
>
> -   x -> version de la aplicacion, se producen cambion > sustaciales
> -   z -> versión minor, se añaden nuevas funcionalidades
> -   j -> versión patch coreccion de errores

---

## Indice

-   [1.0.1](#release-101)
-   [1.0.0](#release-100)

## Release 1.0.1

### Fixed

-   Se corrige error en el la generacion de los tipos para Ts
-   Se corrige error en la publicacion del coverage de test

## Release 1.0.0

### Added

-   Publicadores para console, localstorage, fichero y api.
-   La posibilidad de añadir publicadores externos
-   Documentación
-   Entorno test con cobertura del 93%
