# DeLogger

[![NPM](https://img.shields.io/badge/npm-CB3837?style=for-the-badge&logo=npm&logoColor=white)](https://www.npmjs.com/package/@desenrola/dslogger)
[![GitLab](https://img.shields.io/badge/gitlab-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/BorjaFernandezRodrigo/dslogger)

[![Documentation](https://img.shields.io/badge/view-Documentation-blue)](https://ds-logger-borjafernandezrodrigo-5deef242ae47432500e1b23e22f22d3.gitlab.io/)
[![pipeline status](https://gitlab.com/BorjaFernandezRodrigo/dslogger/badges/main/pipeline.svg)](https://gitlab.com/BorjaFernandezRodrigo/dslogger/-/commits/main)
[![coverage report](https://gitlab.com/BorjaFernandezRodrigo/dslogger/badges/main/coverage.svg)](https://gitlab.com/BorjaFernandezRodrigo/dslogger/-/commits/main)
[![Latest Release](https://gitlab.com/BorjaFernandezRodrigo/dslogger/-/badges/release.svg)](https://gitlab.com/BorjaFernandezRodrigo/dslogger/-/releases)

> **Una libreria para node de registro de log asincrono para varios publicadores.**

# Motivación

DsLogger esta diseñada para ser una librería de registro simple y universal (tanto back como front) con soporte para múltiples publicadores.

Un publicador es un servicio de almacenamiento para los registros. Cada instancia de DsLogger puede tener varios publicadores, con niveles de log distintos. Por ejemplo, seria posible tener un publicador de tipo fichero para que se registren los log a nivel debug y otro publicador también de tipo fichero que permita registrar solo los log de error.

# Uso

```bash
  [sudo] npm install @desenrola/dslogger
```

### Usando DsLogger sin configurar

```ts
import { DsLogger } from '@desenrola/ds-loger';

const logger = new DsLogger();
logger.info('Test message');
```

### Añadiendo publicadores

> Por defecto el logger no trae ningun publicador cargado.

```ts
import { ConsolePublisher, ApiPublisher, LocalStoragePublisher, FilePublisher, DsLogger } from '@desenrola/ds-loger';

const consolePublisher = new ConsolePublisher('all');
const apiPublisher = new ApiPublisher('dabug');
const localStoragePublisher = new LocalStoragePublisher('info');
const filePublisher = new FilePublisher('error');

const logger = new DsLogger()
    .addPublisher(consolePublisher)
    .addPublisher(apiPublisher)
    .addPublisher(localStoragePublisher)
    .addPublisher(filePublisher);

logger.info('Test message');
```

### Añadiendo formato de salida

```ts
import { ConsolePublisher, DsLogger } from '@desenrola/ds-loger';

const consolePublisher = new ConsolePublisher('all');
const logger = new DsLogger({ format: 'string' || 'JSON', withDate: true || false }).addPublisher(consolePublisher);
logger.info('Test message');
```

### Añadiendo metadatos al registro

```ts
import { ConsolePublisher, ISetting, DsLogger } from '@desenrola/ds-loger';

const config: ISetting = {
    tags: [{ tag1: 'value1' }, { tag2: 'value2' }],
};
const consolePublisher = new ConsolePublisher('all');
const logger = new DsLogger(config).addPublisher(consolePublisher);
logger.info('Test message');
```

### Limpiando el stack del registrador

```ts
import { ConsolePublisher, DsLogger } from '@desenrola/ds-loger';

const consolePublisher = new ConsolePublisher('all');
const logger = new DsLogger().addPublisher(consolePublisher);
logger.info('Test message');
logger.clear();
```

# Interfaces

```ts
export type ILevel = 'all' | 'debug' | 'info' | 'notice' | 'warn' | 'error' | 'crit' | 'alert' | 'emerg';

export interface IPublisher {
    log: (msg: JSON | string, level: ILevel) => void;
    clear: () => void;
    canHandler: (level: ILevel) => boolean;
}

export interface ITagPrimitive {
    name: string;
    value: string;
}

export interface ISettingPrimitives {
    format?: IFormat;
    withDate?: boolean;
    tags?: ITagPrimitive[];
}
```

# Instalación

Instalación de npm

```bash
  curl https://npmjs.org/install.sh | sh
```

instalando dslogger

```bash
  [sudo] npm install @desenrola/dslogger
```

# Ejecutar pruebas

Todas las pruebas de DsLogger están escritas en jest y diseñadas para ejecutarse con npm.

```bash
npm test
```

# Desarrolladores

Autor: [Borja Fernández Rodrigo](https://gitlab.com/BorjaFernandezRodrigo)

Contribuidores: [Desenrola](https://gitlab.com/desenrolapp)
